#pragma once
#include "HeaderFiles.h"

struct User                         //credentials of user
{
    char  userName[20];             // user name
    char password[20];             // passsword
    void registerUser();           // function to carry sign in operation
    bool loginUser(char*,char*);  //function to carry login operation

};

struct userDetails {       //meta data of each user
    char userName[20];
    char password[20];
    char name[10];
    char email[20];
    char location[20];
    int no_of_projects;
    userDetails() :no_of_projects(0) {}
};

bool checkUserName(const char*);   //function to check wheather entered username exists
bool emailCheck(string);            //function to carry email validation
void adminLog();                   //function to perform admin operations (display existing users,details of specific user)


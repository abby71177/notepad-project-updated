#include "User-Authentication.h"
#include"Project-Details.h"


void userMenu(char[]);

struct User Obj;
void signUP() {
    int t;
    cout << "\nEnter Registration Details for User  \n";
    Obj.registerUser();

}
void signIN() {
    char newUserName[10], newPassword[10];
    cout << "\nEnter user name :: ";
    cin >> newUserName;

    cout << "\nEnter password :: ";
    cin >> newPassword;
    if (Obj.loginUser(newUserName, newPassword) == 1)
    {
        cout << "Welcome " << newUserName << endl;
        //currentUser = newUserName;
        userMenu(newUserName);
    }

    else
    {

        if (checkUserName(newUserName) == 1) {
            cout << "\Wrong Password\nRe-Enter Details\n";
            signIN();
        }
        else {
            cout << "\nNew User, Do create an account before Logging in,\n";
            signUP();
        }
    }
}

void userMenu(char current[20]) {
    int choice;
    char ch;
    char* currentUser = current;

    do {
        cout << "\n...........Menu.......... \n\n1.Create Project\n2.Append Data to a Project\n3.Update an existing Project\n4.Remove data from existing Project\n5.Display Project Contents\n6.Revert to a version of Project\n7.Details of Project\n\n";
        cout << "Enter your choice: ";
        cin >> choice;
        switch (choice) {
        case 1:createProject(current);break;
        case 2: extendProject(current);break;
        case 3: updateContent(current);break;
        case 4:removeContent(current);break;

        case 5: displayContents(current); break;
        case 6: revertVersion(current); break;
        case 7: projectInfo(current); break;
        //case 7: displayAllProjects();break;
        default: cout << "Invalid choice"; break;

        }
        std::cout << "\nDo you wish to continue with the notepad operations " << currentUser << " (y/n)? :: ";
        std::cin >> ch;
    } while (ch == 'y' || ch == 'Y');

}


   


int main()
{
    int choice;
    char ch;
    do {
        cout << "\n...........Menu.......... \n\n1.Admin Log\n2.Sign Up\n3.Log In\n4.Log Out\n";
        cout << "Enter your choice: ";
        cin >> choice;
        switch (choice) {
        case 1: adminLog();break;
        case 2: signUP();break;
        case 3: signIN();break;
        default: exit(0);
        }


        cout << "\nDo you wish to continue working on Notepad Project (y/n)? :: ";
        cin >> ch;
    } while (ch == 'y' || ch == 'Y');


    //cin.get();

    return 0;
}


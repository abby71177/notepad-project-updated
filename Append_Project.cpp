#include "Project-Details.h"
void extendProject(char * directory) {             
    char fileName[20];
    string path;

    cout << "\nEnter the file name in which you want to add content :: ";
    cin >> fileName;
    ofstream fout;
    ifstream fin;
    path += directory;
    path += "/";
    path += fileName;
    fin.open(path);
    fout.open(path, ios::app);
    if (!fin)
        cout << "\nNo such file exists\n";
    else {

        if (fin.is_open()) {

            fout << fileInput();
        }
        cout << "\nData has been appended to file\n";
        fin.close();
        fout.close();

        updateOperations(path, 1);
        checkVersion(path,fileName,directory);
    }

}

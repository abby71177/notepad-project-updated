#include "Project-Details.h"
void removeContent(char * directory) {                 //to remove contents of project
    int choice;
    char fileName[20];
    cout << "Enter file name :: ";
    cin >> fileName;
    string path("");
    path += directory;
    path += "/";
    path += fileName;
    cout << "\nTo remove contents of a project, Enter\n1.Remove contents of file completely\n2.To remove particular line  \n";
    cin >> choice;
    if (choice == 1) {
        ofstream ofs;
        ofs.open(path, ofstream::out | ofstream::trunc);
        if (!ofs)
        {
            cout << "\nUnable to open file to read\n" << path;
        }
        ofs.close();
        cout << "Contents of file " << fileName << " were removed\n";

    }
    else {
        string deleteline;
        string line;
        cin.get();
        cout << "enter the line which you want to remove :: ";
        getline(cin, deleteline);
        string path2("");
        path2 += directory;
        path2 += "/";
        path2 += "temp.txt";
        ifstream fin;
        fin.open(path);
        ofstream temp;
        temp.open(path2);
        while (getline(fin, line))
        {
            
            if (line != deleteline)
                temp << line << "\n";
        }

        temp.close();
        fin.close();
        char newFile[20], oldFile[20];
        strcpy_s(oldFile, path.c_str());
        strcpy_s(newFile, path2.c_str());
        remove(oldFile);
        rename(newFile, oldFile);
        cout << "\nRequested line was removed from the file\n";


    }
    updateOperations(path, 1);
    checkVersion(path,fileName,directory);



}


#include "User-Authentication.h"
#include"global.h"


char* currentUser;

bool checkUserName(const char* user) {
    ifstream filein;
    struct userDetails userDetObj;
    filein.open("User-Details.txt", ios::in | ios::binary);
    if (!filein)
    {
        std::cout << "\nUnable to open file to read\n";
    }
    else
    {

        filein.read((char*)&userDetObj, sizeof(userDetObj));
        while (filein)
        {
            if (_strcmpi(userDetObj.userName, user) == 0) {
                return true;
            }

            filein.read((char*)&userDetObj, sizeof(userDetObj));
        }
        return false;
        filein.close();
    }
}

bool emailCheck(string email)                       //EMAIL VALIDATION
{
    const regex pattern("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
    //const regex pattern("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
    return regex_match(email, pattern);
}

void User::registerUser()                        // to register a new user
{

    userDetails userobj;
    char uName[20], pass[20];
    User obj;
    cout << "\nEnter user name :: ";
    cin >> obj.userName ;
    //strcpy_s(obj.userName, uName);
    //currentUser = obj.userName;
    strcpy_s(userobj.userName, obj.userName);
    if (checkUserName(obj.userName)) {
        cout << "User Already exists" << endl;
        registerUser();
    }
    else {
        cout << "\nEnter your name :: ";
        cin >> userobj.name;
        cout << "\nEnter your emailId :: ";
        cin >> userobj.email;
        if (emailCheck(userobj.email) == 0) {
            cout << "\nInvalid email\nEnter your emailId :: ";
            cin >> userobj.email;
        }
        cout << "\nEnter your location :: ";
        cin >> userobj.location;
        cout << "\nEnter password :: ";
        cin >> obj.password ;
        strcpy_s(userobj.password, obj.password);

        //cout << obj.userName << " " << obj.password;
        //strcpy_s(obj.userName, userobj.userName);
        ofstream filout, fileout2;
        filout.open("user-list.txt", ios::app);
        fileout2.open("User-Details.txt", ios::app | ios::binary);
        //filout.open("users.txt", ios::app | ios::binary);


        if (!filout || !fileout2)
        {
            cout << "\nCannot open file\n";
        }
        else

        {
            userobj.no_of_projects = 0;

            cout << "\n";
            filout.write((char*)&obj, sizeof(obj));
            fileout2.write((char*)&userobj, sizeof(userDetails));
            _mkdir(userobj.userName);

            filout.close();
            fileout2.close();


            cout << "\n...........You are now registered.......... \n\n";


        }
    }


}




bool User::loginUser(char* user, char* password) {             //to login an existing user
    struct userDetails userDetObj;
    int flag = 0;

    ifstream filein;
    filein.open("User-Details.txt", ios::in | ios::binary);
    if (!filein)
        std::cout << "\nUnable to open file to read\n";
    
    else
    {
        filein.read((char*)&userDetObj, sizeof(userDetObj));
        while (filein)
        {
            if (_strcmpi(userDetObj.userName, user) == 0 && _strcmpi(userDetObj.password, password) == 0) {
                currentUser = userDetObj.userName;
                flag = 1;
                return true;
            }
            filein.read((char*)&userDetObj, sizeof(userDetails));
        }
        if (flag == 0)
            return false;
        filein.close();
    }

}




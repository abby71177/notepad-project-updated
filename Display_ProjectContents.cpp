#include "Project-Details.h"
void displayContents(char* directory) {
    fstream newfile;
    char fileName[10];
    int lineNo = 1;
    cout << "Enter the file name :: ";
    cin >> fileName;
    string path("");
    path += directory;
    path += "/";
    path += fileName;
    newfile.open(path, ios::in);
    if (newfile.is_open()) {
        string tp;
        while (getline(newfile, tp)) {
            cout << lineNo << " " << tp << "\n";
            lineNo++;
        }
        newfile.close(); //close the file object.
    }



}

void projectInfo(char* directory) {               // to display project details of a particular product
    int found = 0;
    string file;
    struct Project projectOBJ;
    cin.get();
    cout << "\nEnter file ID :: ";
    getline(cin, file);
    string project = directory;
    project+= "/";
    project += file;
    //cout << project;
    char newPath[20];
    strcpy_s(newPath, project.c_str());
    ifstream filein;
    filein.open("Project.txt", ios::in);
    filein.read((char*)&projectOBJ, sizeof(projectOBJ));
    //while (filein)
    while (!filein.eof())
    {
        if (projectOBJ.id==file)
        {
            found = 1;
            cout << "\Project Id :: " << projectOBJ.id;
            //cout << "\nProject Name :: " << projectOBJ.projectName;
            cout << "\nNo of Operations done so far :: " << projectOBJ.no_of_operations;
            cout << "\nNo of Versions of this project  created so far ::" << projectOBJ.no_of_versions << "\n\n";
            break;
        }
        else
            filein.read((char*)&projectOBJ, sizeof(projectOBJ));
    }
    if (found == 0)
        cout << "\nNo such file exists\n";
    cin.get();
    filein.close();
    //displayAllProjects();
}

void displayAllProjects() {

    ifstream filein;
    Project projectOBJ;
    filein.open("Project.txt", ios::in);
    filein.read((char*)&projectOBJ, sizeof(projectOBJ));
    cout << "\nList of all Projects and their details\n";
    //while (filein)
    while(!filein.eof())
    {
        
        cout << "\Project Id :: " << projectOBJ.id;
        
        cout << "\nNo of Operations done so far :: " << projectOBJ.no_of_operations;
        cout << "\nNo of Versions of this project  created so far ::" << projectOBJ.no_of_versions << "\n";

        

        filein.read((char*)&projectOBJ, sizeof(projectOBJ));
    }
    filein.close();
    //cin.get();
    
    
}

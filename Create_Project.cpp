#include "Project-Details.h"
#include "User-Authentication.h"

char* CurrentUser;
struct userDetails userOBJ;
struct Project projectObj;

    
string generateId() {            // to generate a randome id
    string Id("");
    Id += CurrentUser;
    int randomNumber = (rand() % 100) + 1;
    Id += to_string(randomNumber);
    return Id;
}

void addProjectData() {           //to add meta data of project into file
    ofstream filout;
    filout.open("Project.txt", ios::app);

    filout.write((char*)&projectObj, sizeof(projectObj));
    filout.close();
}

int returnProjectCount(char userName[10]) {             //to return no of projects created so far
    ifstream filein;
    filein.open("User-Details.txt", ios::in | ios::binary);
    if (!filein)
    {
        std::cout << "\nUnable to open file to read\n";
    }
    else
    {
        filein.read((char*)&userOBJ, sizeof(userDetails));
        while (filein)
        {
            if (strcmp(userOBJ.userName, userName) == 0) {
                return userOBJ.no_of_projects;
            }
            //fileout2.write((char*)&userObj, sizeof(userDetails));
            filein.read((char*)&userOBJ, sizeof(userDetails));
        }

        filein.close();
    }
    return 0;
}

void updateProjectCount(char userName[10]) {  //1->no_of_projects 2->operation count 3->version count
    ifstream filein;
    filein.open("User-Details.txt", ios::in | ios::binary);
    ofstream filout;
    fstream file;

    file.open("User-Details2.txt", ios::out);
    file.close();
    filout.open("User-Details2.txt", ios::app | ios::binary);

    filein.read((char*)&userOBJ, sizeof(userOBJ));
    while (filein)
    {
        if (_strcmpi(userOBJ.userName, userName) == 0)
            userOBJ.no_of_projects += 1;


        filout.write((char*)&userOBJ, sizeof(userOBJ));
        filein.read((char*)&userOBJ, sizeof(userOBJ));

    }
    filein.close();
    filout.close();
    remove("User-Details.txt");
    rename("User-Details2.txt", "User-Details.txt");
}

string fileInput() {
    string newLine;
    cout << "enter line to append \n";
    // cin >> newLine;
    cin.get();
    getline(cin, newLine);
    newLine += "\n";
    return newLine;

}

void createProject(char* directory) {           // to create a project for user
    int stop;
    CurrentUser = directory;
    int projectCount = returnProjectCount(directory);
    string path("");
    path += directory;
    path += "/";
    projectCount++;
    updateProjectCount(directory);
    string filename("file");
    filename += to_string(projectCount);
    filename += ".txt";
    //projectObj.id = generateId();
    projectObj.id = "";
    projectObj.id += directory;
    projectObj.id += to_string(projectCount);
    cout << "\n\n" << filename << " is created whose Id is " << projectObj.id << "\n";


    path += filename;
    ofstream MyFile(path);
    cout << "Enter 1 to keep adding sentences to the file and 0 to terminate\n";
    do {
        MyFile << fileInput();

        cin >> stop;
    } while (stop != 0);
    MyFile.close();

    //char newFile[20], oldFile[20];
    strcpy_s(projectObj.projectName, path.c_str());
    //projectObj.projectName = path;
    projectObj.no_of_operations = 0;
    projectObj.no_of_versions = 0;
    //cout << projectObj.id << projectObj.projectName << projectObj.no_of_operations << projectObj.no_of_versions;
    ofstream filout;
    filout.open("Project.txt", ios::app | ios::binary);

    filout.write((char*)&projectObj, sizeof(projectObj));
    filout.close();
    cin.get();
}


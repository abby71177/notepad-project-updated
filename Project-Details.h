#pragma once
#include "HeaderFiles.h"

struct version {
    string id;

};

struct Project {                 // to store meta data of each project created by user
    string id;                  //username followed by random number.
    char  projectName[20];     //name of project(path)
    int no_of_operations;     //no of operationd done in a single project
    int no_of_versions;      // no of versions of the project created
    version versionObj;     //Version object 

};

void projectInfo();            //to display details of project into file
void addProjectData();         //to append details of project into file
string generateId();      
int returnProjectCount(char userName[10]);   //to return the no of projects created for a user
void updateProjectCount(char userName[10]);  // to increment project count for a user
string fileInput();
void createProject(char* );                //function to create a project on user request
void extendProject(char*);                 //OPERATION 1- to append data to existing project      
void updateContent(char*);                 // OPEARTION 2- to Update contents of existing project
void removeContent(char*);                // OPERATION -3 to remove content from an existing project

void displayContents(char* );           //to display contents of existing project
void projectInfo(char* );               // to display details of existing project
void displayAllProjects();

int returnOperationCount(string , int );    // function to return No of operations done in a project
void updateOperations(string, int);        // function to update the No Of Operations done in a project

void checkVersion(string , char* , char* ); //function to check wheather a version of a project is to be created
void revertVersion(char* );                 //function to revert version of a project
void versionLog(string Id);                // function to store version details in a file
void displayVersions();                   // to display all the versions created so far.


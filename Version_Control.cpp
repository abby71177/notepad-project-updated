#include "Project-Details.h"

void versionLog(string Id) {
    ofstream ofs;
    ofs.open("Versions.txt",ios::app);
    ofs << "\n";
    ofs << Id;
    
    ofs.close();

}
void displayVersions() {
    string line;
    ifstream ifs;
    ifs.open("Versions.txt");
    while (getline(ifs,line) ){
        cout << line << "\n";
    }
    ifs.close();

}


void checkVersion(string name ,char *fileName,char *directory) {                //to check if version of a project is to be created
    int operation = returnOperationCount(name, 1);
    if (operation % 10 == 0)
    {

        cout << "\nVersion of project is created!!\n";
        string path, line;
        path += directory;
        path += "/";
        path += "Version-";
        path += to_string(operation / 10);
        path += "-";
        path += fileName;
        //ofstream MyFile(path);
        ifstream fin;
        fin.open(name);
        ofstream fout;
        fout.open(path);
        while (getline(fin, line)) {

            fout << line << "\n";
        }
        updateOperations(name, 2);
        versionLog(path);

        fout.close();
        fin.close();


    }
}

void revertVersion(char * user) {              //to revert to a version of project
    int versionNo;
    string file, path, path2;
    cin.get();
    cout << "\nEnter the file name you want to revert :: ";
    getline(cin, file);
    cout << "\nEnter the version number you want to revert the project back to :: ";
    cin >> versionNo;
    path += user;
    path += "/";

    path2 = path;
    path += file;
    //cout << path;

    int currentVersion = returnOperationCount(path, 2);

    if (versionNo > currentVersion)
        cout << "\nInvalid Version Number/ Version not yet created\n";
    else {
        path2 += "Version";
        path2 += "-";
        path2 += to_string(versionNo);
        path2 += "-";
        path2 += file;
        //cout << path2;
        
        ofstream ofs;
        ofs.open(path, ofstream::out | ofstream::trunc);
        ofs.close();
        string line;
        ifstream fin;
        fin.open(path2);
        ofstream temp;
        temp.open(path);
        getline(fin, line);
        while (fin) {
            temp << line << "\n";
            getline(fin, line);
        }
        fin.close();
        temp.close();

        cout << "\nRevertion successfully executed\n";


    }
}

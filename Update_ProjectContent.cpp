#include "Project-Details.h"

void updateContent(char * directory) {         //To Update Contents Of the Project either overwrite or update particular line
    int choice, lineNo;
    string line, updateLine, replaceLine;
    char fileName[20];
    cout << "\nTo Update Contents Of the Project\n1.To overwrite the entire project\n2.To update a particular line\nenter choice ::";
    cin >> choice;
    cout << "Enter file name :: ";
    cin >> fileName;
    string path("");
    path += directory;
    path += "/";
    path += fileName;
    cin.get();
    if (choice == 1) {
        ofstream myfile;
        myfile.open(path);
        cout << "Enter line to overwrite the file with :: ";
        getline(cin, line);

        myfile << line << "\n";
        myfile.close();

    }
    else {
        cout << "Enter line the line to be updated :: ";
        getline(cin, updateLine);
        cout << "Enter line the line to be replaced with :: ";
        getline(cin, replaceLine);
        string path2("");
        path2 += directory;
        path2 += "/";
        path2 += "temp.txt";
        ifstream fin;
        fin.open(path);
        ofstream temp;
        temp.open(path2);

        while (getline(fin, line))
        {
            //line.replace(line.find(deleteline), deleteline.length(), "");
            if (line != updateLine)
                temp << line << "\n";
            else
                temp << replaceLine << "\n";
        }

        temp.close();
        fin.close();
        char newFile[20], oldFile[20];
        strcpy_s(oldFile, path.c_str());
        strcpy_s(newFile, path2.c_str());
        remove(oldFile);
        rename(newFile, oldFile);
        cout << "\nRequested line was updated\n";


    }
    updateOperations(path, 1);
    checkVersion(path,fileName,directory);

}
